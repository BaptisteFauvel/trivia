import {expect} from 'chai';
import {describe, it} from 'mocha';
import {GameRunner} from '../src/game-runner';
import {Game} from "../src/game";

describe('The test environment', () => {
    it("The game goes without problem", function (){
        const game =  new Game(true);
        game.addPlayer("billy");
        game.addPlayer("topiqueur");
        game.addPlayer("prinplouf");
        game.addPlayer("Jack");
        game.addPlayer("Timmy");
        try {
            game.start();
        }catch (err) {
            expect(err.message).to.eql("game should have at least 2 players");
        }
    })

    it("The game start with a changed goal", function (){
        const game =  new Game(false, 1);
        game.addPlayer("billy");
        game.addPlayer("topiqueur");
        game.addPlayer("prinplouf");
        game.addPlayer("Jack");
        game.addPlayer("Timmy");
        try {
            game.start();
        }catch (err) {
            expect(err.message).to.eql("game should have at least 2 players");
        }
    })


    it("Should be lower than 6", function (){
        console.log('// Should be lower than 6 //')
        const game =  new Game(false);
        game.addPlayer("billy");
        game.addPlayer("topiqueur");
        game.addPlayer("prinplouf");
        game.addPlayer("Jack");
        game.addPlayer("Timmy");
        game.addPlayer("Chien");
        try{
            game.addPlayer("Chat")
        }catch (err) {
            expect(err.message).to.eql("game should have at most 6 players");
        }
    });
    it("Should be more player than 2",function (){
        console.log('// Should be more player than 2 //')
        const game =  new Game(false);
        game.addPlayer("billy");
        try{
            game.start();
        }catch(err){
            expect(err.message).to.eql("game should have at least 2 players");
        }
    });

    it("The game with stats", function (){
        const game =  new Game(false);
        game.addPlayer("billy");
        game.addPlayer("topiqueur");
        game.addPlayer("prinplouf");
        game.addPlayer("Jack");
        game.addPlayer("Timmy");
        game.addPlayer("chien");
        game.stats();
    })
});
