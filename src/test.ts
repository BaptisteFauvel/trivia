import { Game } from './game';

export class GameRunner {
    public static main(): void {
        try {
            const isRockParty = true;
            const game = new Game(isRockParty, 10);
        
            game.addPlayer("[1]");
            game.addPlayer("[2]");
            game.addPlayer("[3]");
            game.addPlayer("[4]");
            game.addPlayer("[5]");
            game.addPlayer("[6]");
            // game.addPlayer("[7]");

            game.stats();

        } catch (e) {
            console.log(e.message)
        }
    }
}

GameRunner.main();
