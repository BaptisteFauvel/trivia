import { Colors } from './colors'

const USE_JOKER_RATIO = 0.1
const USER_LEAVING_RATIO_PER_TURN = 0
const NUMBER_OF_QUESTIONS_PER_CATERGORY = 2

interface Player {
    inPenaltyBox: boolean,
    strike: number,
    joker: number,
    jailCount: number,
    places: number,
    purses: number,
    name: string,
}

enum questionsCategories {
    Pop = 'Pop',
    Rock = 'Rock',
    Science = 'Science',
    Sports = 'Sports',
    Techno = 'Techno'
}

const questionsCategoriesArr = [
    questionsCategories.Pop,
    questionsCategories.Rock,
    questionsCategories.Science,
    questionsCategories.Sports,
    questionsCategories.Techno,
]

function genRand(max = 1, floored = false): number {
    return floored
    ? Math.floor(Math.random() * max)
    : Math.random() * max
}

function getRandomCategory(): questionsCategories {
    return questionsCategoriesArr[genRand(5, true)]
}

function genArray(size) {
    const arr = [];
    for(let i = 0; i < size; i++) {
        arr.push(null);
    }
    return arr
}

function colorize(string: string, ...colors: Colors[]): string {
    return colors.join() + string + Colors.Reset;
}

export class Game {
    private players: Array < Player > = [];
    private currentPlayer: number = 0;
    private isRockeParty = true;
    private popQuestions: Array < string > = [];
    private scienceQuestions: Array < string > = [];
    private sportsQuestions: Array < string > = [];
    private rockQuestions: Array < string > = [];
    private technoQuestions: Array < string > = [];
    private goldsToWin: number = 6;
    private nextCategory: questionsCategories = null;
    private leaderBoard: { [key: number]: string } = {
        1: null,
        2: null,
        3: null,
    }

    constructor(isRockParty = false, goldsToWin = 6) {
        this.goldsToWin = goldsToWin
        this.isRockeParty = isRockParty
        this.popQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Pop} Question ${i}`)
        this.scienceQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Science} Question ${i}`)
        this.sportsQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Sports} Question ${i}`)
        this.rockQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Rock} Question ${i}`)
        this.technoQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Techno} Question ${i}`)
    }

    private reShuffleDecks() {
        if (this.popQuestions.length <= 0) {
            console.log(colorize('reshuffle pop Questions', Colors.FgMagenta));
            this.popQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Pop} Question ${i}`)
        }
        if (this.scienceQuestions.length <= 0) {
            console.log(colorize('reshuffle science Questions', Colors.FgMagenta));
            this.scienceQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Science} Question ${i}`)
        }
        if (this.sportsQuestions.length <= 0) {
            console.log(colorize('reshuffle sports Questions', Colors.FgMagenta));
            this.sportsQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Sports} Question ${i}`)
        }
        if (this.rockQuestions.length <= 0) {
            console.log(colorize('reshuffle rock Questions', Colors.FgMagenta));
            this.rockQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Rock} Question ${i}`)
        }
        if (this.technoQuestions.length <= 0) {
            console.log(colorize('reshuffle techno Questions', Colors.FgMagenta));
            this.technoQuestions = genArray(NUMBER_OF_QUESTIONS_PER_CATERGORY).map((_, i) => `${questionsCategories.Techno} Question ${i}`)
        }
    }

    public addPlayer(name: string): void {
        if (this.players.length === 6) {
            throw new Error('game should have at most 6 players')
        }

        const player = {
            inPenaltyBox: false,
            jailCount: 0,
            strike: 0,
            joker: 1,
            places: 0,
            purses: 0,
            name,
        }

        this.players.push(player);

        console.log(Colors.FgCyan, name + Colors.Reset + " was added");
        console.log("They are player number " + this.players.length);
    }

    private roll() {
        const roll = genRand(6, true) + 1
        console.log(colorize(this.players[this.currentPlayer].name, Colors.FgCyan) + " is the current player, he rolled a " + colorize(roll.toString(10), Colors.FgBlue));

        if (this.players[this.currentPlayer].inPenaltyBox) {
            console.log(colorize(this.players[this.currentPlayer].name, Colors.FgCyan) + colorize(" has 1 chance over " + this.players[this.currentPlayer].jailCount, Colors.FgRed) + " to get out of the jail")
            if (genRand() < 1 / this.players[this.currentPlayer].jailCount) {
                console.log(colorize(this.players[this.currentPlayer].name, Colors.FgCyan) + " is getting out of the penalty box");
                this.players[this.currentPlayer].inPenaltyBox = false
            } else {
                console.log(colorize(this.players[this.currentPlayer].name, Colors.FgCyan) + " is not getting out of the penalty box");
            }
        }

        if (!this.players[this.currentPlayer].inPenaltyBox) {
            this.players[this.currentPlayer].places = (this.players[this.currentPlayer].places + roll) % 12;

            console.log(colorize(this.players[this.currentPlayer].name, Colors.FgCyan) + "'s new location is " + this.players[this.currentPlayer].places);
            console.log("The category is " + this.currentCategory());
            this.askQuestion();
        }
    }

    private askQuestion(): void {
        if (this.currentCategory() === questionsCategories.Pop)
            console.log(this.popQuestions.shift());
        if (this.currentCategory() === questionsCategories.Science)
            console.log(this.scienceQuestions.shift());
        if (this.currentCategory() === questionsCategories.Sports)
            console.log(this.sportsQuestions.shift());
        if (this.currentCategory() === questionsCategories.Rock)
            console.log(this.rockQuestions.shift());
        if (this.currentCategory() === questionsCategories.Techno)
            console.log(this.technoQuestions.shift());
        this.reShuffleDecks()
    }

    private currentCategory(): string {
        if (this.nextCategory) {
            return this.nextCategory
        }
        const gameBoard = {
            0: questionsCategories.Pop,
            4: questionsCategories.Pop,
            8: questionsCategories.Pop,
            1: questionsCategories.Science,
            5: questionsCategories.Science,
            9: questionsCategories.Science,
            2: questionsCategories.Sports,
            6: questionsCategories.Sports,
            10: questionsCategories.Sports,
        }

        if (gameBoard[this.players[this.currentPlayer].places]) {
            return gameBoard[this.players[this.currentPlayer].places]
        } else {
            return this.isRockeParty ? questionsCategories.Rock : questionsCategories.Techno
        }
    }

    private setPlayerIntoLeaderBoard() {
        const player = this.players.splice(this.currentPlayer, 1)[0]
        if (!this.leaderBoard[1]) {
            this.leaderBoard[1] = player.name
        } else
        if (!this.leaderBoard[2]) {
            this.leaderBoard[2] = player.name
        } else
        if (!this.leaderBoard[3]) {
            this.leaderBoard[3] = player.name
        }
        this.currentPlayer = this.currentPlayer % this.players.length
    }

    private isLeaderBoardFull(): boolean {
        if (this.players[this.currentPlayer].purses >= this.goldsToWin) {
            this.setPlayerIntoLeaderBoard()
        }
        if (this.players.length === 1) {
            this.setPlayerIntoLeaderBoard()
        }
        return (!!this.leaderBoard[1] && !!this.leaderBoard[2] && !!this.leaderBoard[3])
            || this.players.length < 2
    }

    private wrongAnswer(): void {
        console.log(colorize('Question was incorrectly answered', Colors.FgYellow));
        console.log(colorize(this.players[this.currentPlayer].name, Colors.FgCyan) + " was sent to the penalty box and reset his combo");
        this.players[this.currentPlayer].inPenaltyBox = true;
        this.players[this.currentPlayer].jailCount += 1
        this.nextCategory = getRandomCategory()
        console.log("the next category is " + colorize(this.nextCategory, Colors.FgBlue))
    }

    private correctAnswer(): void {
        console.log(colorize('Answer was correct!!!!', Colors.FgGreen));
        this.players[this.currentPlayer].strike += 1
        this.players[this.currentPlayer].purses += this.players[this.currentPlayer].strike;
        console.log(Colors.FgCyan + this.players[this.currentPlayer].name + Colors.Reset + " earn " +
            colorize(this.players[this.currentPlayer].strike.toString(10), Colors.FgBlue) + " Golds and now has " +
            colorize(this.players[this.currentPlayer].purses.toString(10), Colors.FgBlue) + " Gold Coins.");
    }

    private usesJoker(): boolean {
        if (this.players[this.currentPlayer].joker > 0) {
            if (genRand() < USE_JOKER_RATIO) {
                console.log(colorize(this.players[this.currentPlayer].name, Colors.FgCyan) + " uses his joker")
                this.players[this.currentPlayer].joker -= 1
                return true
            }
        }
        return false
    }

    public stats() {
        let count = 0
        const players = {}
        const log = console.log
        console.log = () => null
        while(count < 10000) {
            count++
            this.roll();
            if (!players[this.players[this.currentPlayer].name]) {
                players[this.players[this.currentPlayer].name] = {
                    [questionsCategories.Pop]: 0,
                    [questionsCategories.Rock]: 0,
                    [questionsCategories.Science]: 0,
                    [questionsCategories.Sports]: 0,
                    [questionsCategories.Techno]: 0,
                }
            }
            players[this.players[this.currentPlayer].name][this.currentCategory()] += 1
            this.currentPlayer = (this.currentPlayer + 1) % this.players.length;
        }
        log(players)
        log(count, 'turns')
        questionsCategoriesArr.forEach(value => {
            const total = this.players.reduce((count, { name }) => {
                return count + players[name][value]
            }, 0)
            log('average occurence for ' + value + ':', (total / (Object.keys(players).length * 5)).toString(10))
        })
        
    }

    public start() {
        let noWinner = true;
        while (noWinner) {
            console.log('\n', colorize("--- Next turn ---", Colors.FgRed))
            if (genRand() < USER_LEAVING_RATIO_PER_TURN) {
                console.log(colorize(this.players[this.currentPlayer].name, Colors.FgCyan) + colorize(" is leaving the game \n", Colors.FgMagenta))

                this.players.splice(this.currentPlayer, 1)
                this.currentPlayer = this.currentPlayer % this.players.length
            }

            if (this.players.length < 2) {
                throw new Error('game should have at least 2 players')
            }

            this.roll();

            this.nextCategory = null

            if (!this.players[this.currentPlayer].inPenaltyBox) {
                if (!this.usesJoker()) {
                    if (genRand(10, true) >= 5) {
                        this.wrongAnswer();
                    } else {
                        this.correctAnswer();
                    }
                }
            }

            noWinner = !this.isLeaderBoardFull()
            this.currentPlayer = (this.currentPlayer + 1) % this.players.length;
        }
        console.log(colorize('\nleaderBoard :', Colors.FgGreen))
        console.log(this.leaderBoard)
    }
}
