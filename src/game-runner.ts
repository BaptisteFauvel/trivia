import { Game } from './game';

export class GameRunner {
    public static main(): void {
        try {
            const isRockParty = true;
            const game = new Game(isRockParty, 10);
        
            game.addPlayer("billy");
            game.addPlayer("topiqueur");
            game.addPlayer("prinplouf");
            game.addPlayer("Jack");
            game.addPlayer("Timmy");

            game.start();

        } catch (e) {
            console.log(e.message)
        }
    }
}

GameRunner.main();
